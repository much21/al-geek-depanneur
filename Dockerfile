FROM openjdk:8-jre

COPY /target/al-geek-depanneur-0.0.1-SNAPSHOT.jar algeekrep.jar

ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","/algeekrep.jar"]