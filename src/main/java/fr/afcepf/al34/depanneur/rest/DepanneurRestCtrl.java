package fr.afcepf.al34.depanneur.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

import fr.afcepf.al34.depanneur.entity.Depanneur;
import fr.afcepf.al34.depanneur.service.DepanneurService;


/**
 * Rest service controller for managing CRUD operations on Depanneur
 */
@RestController
@CrossOrigin("*")
@RequestMapping(value="", headers="Accept=application/json")
public class DepanneurRestCtrl {

	@Autowired
	private DepanneurService service;
	
	// URL : http://localhost:8180/depanneur-api
	@GetMapping(value="")
	public List<Depanneur> getAllDepanneurs() {
		return service.getAllDepanneurs();
	}
	
//	@GetMapping(value="")
//	public Depanneur rechercherById(@RequestParam(value="id")Long id) {
//		return service.rechercherById(id);
//	}

	
	// URL : http://localhost:8180/depanneur-api/grenoble
	@GetMapping(value="/{ville}")
	public List<Depanneur> rechercherDepanneurParVille(@PathVariable("ville") String ville) {
		return service.getDepanneurByVille(ville.toLowerCase());
		
	}	
	

	// URL : http://localhost:8180/depanneur-api/add
	@PostMapping(value="/add")
	public Depanneur ajouterUnDepanneur(@Valid @RequestBody Depanneur depanneur) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(depanneur.getAdresse());
		stringBuilder.append(" ");
		stringBuilder.append(depanneur.getVille());
		LatLng latitudeLongitude = getCoordonnees(stringBuilder.toString());
		depanneur.setLat(latitudeLongitude.lat);
		depanneur.setLon(latitudeLongitude.lng);
		return service.addNewDepanneur(depanneur);
		
	}
	
	// http://localhost:8180/depanneur-api/modif
	@PutMapping(value = "/modif")
	public Depanneur modifier(@RequestBody Depanneur depanneur) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(depanneur.getAdresse());
		stringBuilder.append(" ");
		stringBuilder.append(depanneur.getVille());
		LatLng latitudeLongitude = getCoordonnees(stringBuilder.toString());
		depanneur.setLat(latitudeLongitude.lat);
		depanneur.setLon(latitudeLongitude.lng);
		return service.modifier(depanneur);
		
	}
	
	
	// methode technique pour recuperer la latitude et la longitude de l'adresse du new depanneur avant de le persister en bdd
	public LatLng getCoordonnees(String adresse) {
		GeoApiContext contextG = new GeoApiContext.Builder().apiKey("AIzaSyCP-KHyEETzyq1EQXJ48fteX_pL-j2ETeE").build();
		LatLng latitudeLongitude = new LatLng();
		try {
			GeocodingResult[] result = GeocodingApi.geocode(contextG, adresse).await();
			latitudeLongitude = (result[0].geometry.location);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return latitudeLongitude;
	}
}
