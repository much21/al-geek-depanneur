package fr.afcepf.al34.depanneur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * DepanneurApp main class
 */
@SpringBootApplication
public class DepanneurApp {


	public static void main(String[] args) {
		
		SpringApplication app = new SpringApplication(DepanneurApp.class);
		ConfigurableApplicationContext context = app.run(args);
		System.out.println("http://localhost:8180/depanneur");
		System.out.println("TOTO");
		
		
	}
				
}



